"""""""""""""""""""""""
"=> NEOBUNDLE :-)
"""""""""""""""""""""""
  " Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if &compatible
 set nocompatible               " Be iMproved
endif

" Required:
set runtimepath^=~/.config/nvim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('~/.config/nvim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" MY BUNDLES, etcetera

"plugins
NeoBundle "Shougo/deoplete.nvim"
NeoBundle "digitaltoad/vim-pug"
NeoBundle "fatih/vim-go"
NeoBundle "https://github.com/luochen1990/rainbow"
NeoBundle "https://github.com/davisdude/vim-love-docs"
NeoBundle "https://github.com/alols/vim-love-efm.git"
NeoBundle "https://github.com/easymotion/vim-easymotion.git"
"NeoBundle "https://github.com/scrooloose/syntastic.git"
NeoBundle "https://github.com/tpope/vim-fugitive.git"
NeoBundle "https://github.com/scrooloose/nerdtree.git"
NeoBundle "https://github.com/mattn/emmet-vim.git"
NeoBundle "https://github.com/Valloric/YouCompleteMe.git"
NeoBundle "https://github.com/scrooloose/nerdcommenter.git"
"NeoBundle "https://github.com/powerline/powerline.git"
NeoBundle "https://github.com/majutsushi/tagbar.git"
NeoBundle "https://github.com/jistr/vim-nerdtree-tabs.git"
NeoBundle "https://github.com/kien/ctrlp.vim"
NeoBundle "https://github.com/jmcomets/vim-pony/"
NeoBundle "https://github.com/vim-scripts/taglist.vim.git"
"NeoBundle "https://github.com/jshint/jshint.git"
NeoBundle "MattesGroeger/vim-bookmarks"
NeoBundle "Shougo/unite.vim"
NeoBundle "mhartington/vim-typings"
NeoBundle "mxw/vim-jsx"
NeoBundle "pangloss/vim-javascript"

NeoBundle "ternjs/tern_for_vim"
NeoBundle "vim-airline/vim-airline"
NeoBundle "jeetsukumaran/vim-buffergator"
NeoBundle "terryma/vim-multiple-cursors"
NeoBundle "szw/vim-maximizer"
NeoBundle "Raimondi/delimitMate"
NeoBundle "https://github.com/othree/javascript-libraries-syntax.vim"
NeoBundle "https://github.com/MarcWeber/vim-addon-local-vimrc"
NeoBundle "neomake/neomake"
NeoBundle "Shougo/vimproc.vim"
"NeoBundle "thirtythreeforty/lessspace.vim"
NeoBundle "airblade/vim-gitgutter"
NeoBundle "christoomey/vim-tmux-navigator"
NeoBundle "junegunn/goyo.vim"
NeoBundle "junegunn/limelight.vim"
"NeoBundle "bilalq/lite-dfm"

"themes
NeoBundle "https://github.com/NewProggie/NewProggie-Color-Scheme"
NeoBundle "https://github.com/zcodes/vim-colors-basic"
NeoBundle "https://github.com/tokers/Magellan"
NeoBundle "https://github.com/nightsense/seabird"
NeoBundle "https://github.com/zcodes/vim-colors-basic"
NeoBundle "https://github.com/nightsense/seabird"
NeoBundle "https://github.com/rakr/vim-one"
NeoBundle "https://github.com/davidklsn/vim-sialoquent"
NeoBundle "https://github.com/zanglg/nova.vim"
NeoBundle "https://github.com/morhetz/gruvbox"
NeoBundle "https://github.com/Konstruktionist/vim"
NeoBundle "https://github.com/aliou/moriarty.vim"
NeoBundle "https://github.com/KeitaNakamura/neodark.vim"
NeoBundle "https://github.com/Blevs/vim-dzo"
NeoBundle "https://github.com/kamwitsta/nordisk"
NeoBundle "https://github.com/kamwitsta/mythos"
NeoBundle "https://github.com/tyrannicaltoucan/vim-quantum"
NeoBundle "gosukiwi/vim-atom-dark"
NeoBundle "chriskempson/base16-vim"
NeoBundle 'vim-airline/vim-airline-themes'
NeoBundle "altercation/vim-colors-solarized"
NeoBundle "https://github.com/jnurmine/Zenburn.git"
NeoBundle "https://github.com/joshdick/onedark.vim"
NeoBundle "https://github.com/whatyouhide/vim-gotham"
NeoBundle "https://github.com/MvanDiemen/ghostbuster"
NeoBundle "https://github.com/easysid/mod8.vim"
NeoBundle "https://github.com/morhetz/gruvbox"
NeoBundle "https://github.com/jackiehluo/vim-material"
NeoBundle "flazz/vim-colorschemes"
NeoBundle "https://github.com/jacoborus/tender.vim"
NeoBundle "https://github.com/muellan/am-colors"
NeoBundle "https://github.com/andbar-ru/vim-unicon"
NeoBundle "https://github.com/raphamorim/lucario"



call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

"""""""""""""""""""""""
"=> VIM THEMES
"""""""""""""""""""""""

set guifont=Monac:h12
syntax on
set background=dark
"color hybrid
"color harlequin
"color heroku-terminal
"color tender
"color smyck
"color jellybeans
"color busierbee
"color unicon
"color monokain
"color solarized
"color shades-of-teal
"color moriarty
"color Kafka
"color dzo
"color quantum
"color neodark
"color OceanicNext
"color base
"color badwolf
"color material
"color stormpetrel
color github


"""""""""""""""""""""""
"=> VIM STUFF
"""""""""""""""""""""""

let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'

"nnoremap <silent> <BS> :TmuxNavigateLeft<cr>
vnoremap <C-r> "hy:%s/<C-r>h//g<left><left>
tnoremap <Esc> <C-\><C-n>

" Fast window resizing with +/- keys (horizontal); / and * keys (vertical)
if bufwinnr(1)
  map <kPlus> <C-W>+
  map <kMinus> <C-W>-
  map <kDivide> <c-w><
  map <kMultiply> <c-w>>
endif

"Enable the mouse
set mouse=a
"Set utf8 as standard encoding
set encoding=utf8

"Use Unix as the standard file ype
set ffs=unix,dos,mac

"Show line numbers
set number

"set tabwidth to 2
set tabstop=2
set shiftwidth=2

set nowrap
"set colorcolumn=80

"Enable smarttab
set smarttab

set ai "Auto indent
set si "Smart indent

"Use spaces instead of tab
set expandtab

"Enable filetype plugins
filetype plugin on
filetype indent on
set nocompatible

"auto read external file changes
set autoread

"set leader key to ,
let mapleader=","
let g:mapleader = ","

"Fast saving
nmap <leader>w :w!<cr>
nmap <leader>wa :wa!<cr>

"Fast Quit
nmap <leader>q :q<cr>
nmap <leader>qa :qa<cr>

"Fast Write Quit
nmap <leader>wq :wq<cr>
nmap <leader>wqa :wqa<cr>

"Clear trailing white space automatically when exit insert mode
autocmd! BufReadPost,BufWritePost * %s/\s\+$//e<cr>
"Clear trailing white space
nmap <space>s :%s/\s\+$//e<cr>

"Ignore compiled files
set wildignore=*.o,*~,*.pyc

"Always show current position
set ruler

"Height of command bar
set cmdheight=2

"A buffer becomes hidden when it is abandoned
set hid

"Configure backspace to act as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

"Ignore case when searching
set ignorecase

"Enable smartcase
set smartcase

"Highlight search results
"set hlsearch
set hls!

"Make search act like search in modern browsers
set incsearch


" Smart way to move between windows
map <S-j> <C-W>j
map <S-k> <C-W>k
map <S-h> <C-W>h
map <S-l> <C-W>l

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap VIM 0 to first non-blank character
map 0 ^

"Move a line of text using ALT+[jk] or Comamnd+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

"Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove

"Opens a new tab with the current buffer's path
"Super useful when editing files in the same directory
"map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/
map <leader>te :tabedit<space>
map <leader>vs :vsplit<space>
map <leader>sp :split<space>
" Close all buffers
map <leader>bd :bufdo bd<cr>

"Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

"Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

"Enable folding
set foldmethod=indent
set foldlevel=99

"Enable folding with the spacebar
nnoremap <space> za

"python with virtualenv support
py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF

"Show tabs
"set list

"""""""""""""""""""""""
"=> NERDCOMMENTOR
"""""""""""""""""""""""


"""""""""""""""""""""""
"=> TAGBAR
"""""""""""""""""""""""
nmap <F8> :TagbarToggle<CR>


"""""""""""""""""""""""
"=> NERDTREE
""""""""""""""""""""""

"toggle nerd tree with control-n
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeWinPos = "left"

"Hide object files
let NERDTreeIgnore=['\.pyc$', '\~$', '\.o$'] "ignore files in NERDTree



"""""""""""""""""""""""
"=> YCM
"""""""""""""""""""""""

"set min characters for completion to 1
let g:ycm_min_num_of_chars_for_completion = 1



"""""""""""""""""""""""
"=> EMMET CODING
"""""""""""""""""""""""

"expand with control-k
let g:user_emmet_expandabbr_key = '<c-k>'

"""""""""""""""""""""""
"=> POWER LINE
""""""""""""""""""""""
"python from powerline.vim import setup as powerline_setup
"python powerline_setup()
"python del powerline_setup
"set laststatus=2
"set t_Co=256

"""""""""""""""""""""""
"=> SYNTASTIC
""""""""""""""""""""""
"" disable syntastic on java(too slow)
"let g:syntastic_mode_map = {'mode': 'active', 'passive_filetypes':['java', 'asm']}
"let g:syntastic_javascript_checkers = ['eslint']
"let g:syntastic_cpp_checkers = ['clang_check']

"""""""""""""""""""""""
"=> DJANGO STUFF
"""""""""""""""""""""""

map <leader>da :Dadmin
map <leader>du :Durls
map <leader>dv :Dviews
map <leader>ds :Dsettings
map <leader>dm :Dmodels
map <leader>dt :Dtests

"""""""""""""""""""""""
"=> REACTJS STUFF
"""""""""""""""""""""""

"Allow JSX in normal JS files
let g:jsx_ext_required = 0

"""""""""""""""""""""""
"=> VIM-AIRLINE
"""""""""""""""""""""""

set laststatus=2
let g:airline_powerline_fonts = 1
"let g:airline_theme='kolor'
"let g:airline_theme='papercolor'
"let g:airline_theme='cool'
"let g:airline_theme='laederon'
"let g:airline_theme='quantum'
let g:airline_theme='neodark'
"let g:airline_theme='kalisi'
"let g:airline_theme='dzo'

" Enable buffer list
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

"""""""""""""""""""""""
"=> CONTROL-P
"""""""""""""""""""""""

" Setup default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site|node_modules)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in
" version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" Use a leader instead of the actual named binding
"nmap <leader>p :CtrlP<cr>

" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>

"""""""""""""""""""""""
"=> BUFFERGATOR
"""""""""""""""""""""""

" Use the right side of the screen
let g:buffergator_viewport_split_policy = 'R'

" I want my own keymappings...
let g:buffergator_suppress_keymaps = 1

" Looper buffers
"let g:buffergator_mru_cycle_loop = 1

" Go to the previous buffer open
nmap <leader>n :BuffergatorMruCyclePrev<cr>

" Go to the next buffer open
nmap <leader>m :BuffergatorMruCycleNext<cr>

" View the entire list of buffers open
nmap <leader>bl :BuffergatorOpen<cr>

" Shared bindings from Solution #1 from earlier
nmap <leader>e :edit<space>
nmap <leader>bq :bp <BAR> bd #<cr>


"""""""""""""""""""""""
"=> FUGITIVE
"""""""""""""""""""""""

" fugitive git bindings
nmap <leader>ga :Git add %:p<CR><CR>
nmap <leader>gs :Gstatus<CR>
nmap <leader>gc :Gcommit -v -q<CR>
nmap <leader>gt :Gcommit -v -q %:p<CR>
nmap <leader>gd :Gdiff<CR>
nmap <leader>ge :Gedit<CR>
nmap <leader>gr :Gread<CR>
nmap <leader>gw :Gwrite<CR><CR>
nmap <leader>gl :silent! Glog<CR>:bot copen<CR>
nmap <leader>gp :Ggrep<Space>
nmap <leader>gm :Gmove<Space>
nmap <leader>gb :Git branch<Space>
nmap <leader>go :Git checkout<Space>
nmap <leader>gps :!git push<CR>
nmap <leader>gpl :!git pull<CR>
nmap <leader>gx :Git reset HEAD %:p<CR>
nmap <leader>gbl :Gblame<CR>


"""""""""""""""""""""""
"=> GIT GUTTER
"""""""""""""""""""""""
let g:gitgutter_enabled = 0
nmap <leader>gg :GitGutterToggle<CR>
nmap <leader>ggh :GitGutterLineHighlightsToggle<CR>

"""""""""""""""""""""""
"=> MULTIPLE CURSORS
"""""""""""""""""""""""

" Disable default mappings
let g:multi_cursor_use_default_mapping=0

" Remap
let g:multi_cursor_next_key='<C-u>'
let g:multi_cursor_prev_key='<C-i>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<Esc>'

"""""""""""""""""""""""
"=> JS LIB SYNTAX
"""""""""""""""""""""""

let g:used_javascript_libs = ''


"""""""""""""""""""""""
"=> NEOMAKE
"""""""""""""""""""""""

" Run NeoMake on read and write operations
autocmd! BufReadPost,BufWritePost,CursorHold * Neomake
"let g:neomake_javascript_eslint_maker = {
        "\ 'pipe': 1,
        "\ 'args': ['-f', 'compact', '--stdin', '--stdin-filename', '%:p'],
        "\ 'errorformat': '%E%f: line %l\, col %c\, Error - %m,' .
        "\ '%W%f: line %l\, col %c\, Warning - %m'
        "\ }

"autocmd! CursorHold *.js,*.jsx Neomake

let g:neomake_serialize = 1
let g:neomake_serialize_abort_on_error = 1

let g:neomake_javascript_enabled_makers = ['eslint']
let g:neomake_cpp_enabled_makers = ['gcc']

let g:neomake_php_enabled_makers = ['php']


"""""""""""""""""""""""
"=> GOYO
"""""""""""""""""""""""

function! s:goyo_enter()
  silent !tmux set status off
  silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  set noshowmode
  set noshowcmd
  set scrolloff=999
  Limelight
endfunction

function! s:goyo_leave()
  silent !tmux set status on
  silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  set showmode
  set showcmd
  set scrolloff=5
  color github
  let g:airline_powerline_fonts = 1
  let g:airline_theme='neodark'
  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tabline#fnamemod = ':t'
  Limelight!
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
nmap <leader>z :Goyo<cr>



"""""""""""""""""""""""
"=> LiteDFM
"""""""""""""""""""""""
"nnoremap <leader>z :LiteDFMToggle<cr>

"""""""""""""""""""""""
"=> Love2d
"""""""""""""""""""""""

nmap <leader>l :!love .<cr>

"""""""""""""""""""""""
"=> Deoplete
"""""""""""""""""""""""
call deoplete#enable()

autocmd FileType python nnoremap <leader>y :0,$!yapf<Cr>
autocmd CompleteDone * pclose " To close preview window of deoplete automagically